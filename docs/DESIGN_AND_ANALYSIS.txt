Question:Provide a description of the methodology you followed to write your client
and server. Specifically, discuss your selection of data structures for storing and manipulating the
tables and files at the server, etc., and any other important design decisions. Describe the status of
your project. Be honest about what specifications it does or does not meet.


We tried to use a simple yet workable approach when designing this project. We decided that it would be best to use a serve-and-forget type of method, this way the server is lessened of the burden of keeping track of clients and only has to serve whatever requests it's currently being asked for. This of course leaves the retry mechanics to be on the client (so that if it does not hear back from the server within a certain amount of time then it will resend it's query to the server). As far as client locks being removed, we implemented a simple thread timer on the server to stay alive as long as the client was actively talking to the server. When communication stopped (or when a file close event called it) after its set grace period then the thread will automatically remove all file locks held by that client and remove it's information from the client table. This thread then adds itself to be garbage collected and joined and retires.

We chose to use simple vectors from the standard library to hold our information. They are simple yet powerful for storing and retrieving the data. We also used simple structs to hold the data related to each object (Such as the client tables datamembers: clientIP, clientName, requestNumber, requestList, responseList, clientAddr, thread, and lock). This allows us a nice and organized way to store each object. For the UDP communication we used (as per the example) the sys/socket library. The overall communication protocol we used is outlined briefly at the bottom of this document.

This project works very well for the small amount of time (as opposed to a large scale project with a large development team) that we developed it in. It meets all the basic requirements of running on the same computer as the server, running on a different computer as the server, and also running multiple clients on a different computer. This was designed so that it shuld be able to scale to a large number of clients on a single computer. (The server attempts to bind itself to the given port first. If this is unnavailable it incrementally increases until it finds an open port to bind to and communicate with the server on.) The particularly weak point of this project would be if this were to be multiple clients with multiple requests at once.



///////////////////////////////////////////////////////////////////////////////
Standard Connection Protocol Outline:
	Legend:
		<Host>
		[Network Action]
		(Internal Action)


<Client>
(Read Request from script file)
(Read Incarnation Number)
(Increment Request Number)
(Start Response timeout thread, 1 second)
[Make Request]
..
(If Response timeout pops, resend this request)


					<Server>
					[Recieve Request]
					(Generate simulation action:
						- Drop
						- Process but don't send
						- Process normally)
					(Add client if not exist)
					(If Incarnation incremented, release file lock and invalidate request)					
					(Take action based on request Number:
						- r# < last R#, ignore 
						- r# == last R#, resend stored response
						- r# > last R#, serve response)
					(Start/Restart file lock thread timer, 10 seconds:
						- Restart if exists
						- Start if new client)
					[Serve Response]
					..
					(If file lock timer pops, remove file locks and user entry)


<Client>
[Recieve Response]
(Process response)
(Proceed with next script request || exit)