// client.cpp

#include "client.h"

void DieWithError(const char *errorMessage) // error handling: print & exit
{
	perror(errorMessage);
	exit(1);
}

/*******************
Functions called to handle each kind of
request to the server.
-Open, close, read, write, seek, fail
Parameters: request struct with the
appropriate fields filled out prior
to calling the function -- with the exception
of fail.
*******************/
void request_open(Request info, int sock, sockaddr_in servAddr)
{
	// DEBUG CODE
	debug_printRequest(info);
	printf("request_open() called.\n");
	char respBuffer[81];
	respBuffer[0] = 0;
	int test;
	unsigned int fromSize;
	struct sockaddr_in fromAddr;
	printf("Sending request...\n");
	sendto(sendtosock, &info, sizeof(Request), 0, (struct sockaddr *)
			&sendToAddr, sizeof(servAddr));
	for(int x = 0; x < 9; x++)	//Attempts sending 10 times
	{
		for(int y = 0; y < 10; y++)	//Attempts reading 10 times (once every 100 millisec) before re-sending
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(READTIMEOUT));

			if(strlen(respBuffer) == 0){
				fromSize = sizeof(fromAddr);
				recvfrom(sock, respBuffer, 80, MSG_DONTWAIT, (struct sockaddr *) &fromAddr, &fromSize);
			}else{
				printf("Response recieved: "); printf(respBuffer); printf("\n");
				return;
			}
		}
		printf("Response timeout... resending...\n");
		sendto(sendtosock, &info, sizeof(Request), 0, (struct sockaddr *)
				&sendToAddr, sizeof(servAddr));
    }
	printf("Failed to receive a response 10 times in a row...\n");
	printf("Exiting...\n");
	exit(0);
    return;
}


void request_close(Request info, int sock, sockaddr_in servAddr)
{
	// DEBUG CODE
	debug_printRequest(info);
    printf("request_close() called.\n");
    char respBuffer[81];
    	respBuffer[0] = 0;
    	int test;
    	unsigned int fromSize;
    	struct sockaddr_in fromAddr;
    	printf("Sending request...\n");
    	sendto(sendtosock, &info, sizeof(Request), 0, (struct sockaddr *)
    			&sendToAddr, sizeof(servAddr));
    	for(int x = 0; x < 9; x++)	//Attempts sending 10 times
    	{
    		for(int y = 0; y < 10; y++)	//Attempts reading 10 times (once every 100 millisec) before re-sending
    		{
    			std::this_thread::sleep_for(std::chrono::milliseconds(READTIMEOUT));

    			if(strlen(respBuffer) == 0){
    				fromSize = sizeof(fromAddr);
    				recvfrom(sock, respBuffer, 80, MSG_DONTWAIT, (struct sockaddr *) &fromAddr, &fromSize);
    			}else{
    				printf("Response recieved: "); printf(respBuffer); printf("\n");
    				return;
    			}
    		}
    		printf("Response timeout... resending...\n");
    		sendto(sendtosock, &info, sizeof(Request), 0, (struct sockaddr *)
    				&sendToAddr, sizeof(servAddr));
        }
    	printf("Failed to receive a response 10 times in a row...\n");
    	printf("Exiting...\n");
    	exit(0);
        return;
}


void request_read(Request info, int sock, sockaddr_in servAddr)
{
	// DEBUG CODE
	debug_printRequest(info);
    printf("request_read() called.\n");
    char respBuffer[81];
    	respBuffer[0] = 0;
    	int test;
    	unsigned int fromSize;
    	struct sockaddr_in fromAddr;
    	printf("Sending request...\n");
    	sendto(sendtosock, &info, sizeof(Request), 0, (struct sockaddr *)
    			&sendToAddr, sizeof(servAddr));
    	for(int x = 0; x < 9; x++)	//Attempts sending 10 times
    	{
    		for(int y = 0; y < 10; y++)	//Attempts reading 10 times (once every 100 millisec) before re-sending
    		{
    			std::this_thread::sleep_for(std::chrono::milliseconds(READTIMEOUT));

    			if(strlen(respBuffer) == 0){
    				fromSize = sizeof(fromAddr);
    				recvfrom(sock, respBuffer, 80, MSG_DONTWAIT, (struct sockaddr *) &fromAddr, &fromSize);
    			}else{
    				printf("Response recieved: "); printf(respBuffer); printf("\n");
    				return;
    			}
    		}
    		printf("Response timeout... resending...\n");
    		sendto(sendtosock, &info, sizeof(Request), 0, (struct sockaddr *)
    				&sendToAddr, sizeof(servAddr));
        }
    	printf("Failed to receive a response 10 times in a row...\n");
    	printf("Exiting...\n");
    	exit(0);
        return;
}


void request_write(Request info, int sock, sockaddr_in servAddr)
{
	// DEBUG CODE
	debug_printRequest(info);
	printf("request_write() called.\n");
	char respBuffer[81];
		respBuffer[0] = 0;
		int test;
		unsigned int fromSize;
		struct sockaddr_in fromAddr;
		printf("Sending request...\n");
		sendto(sendtosock, &info, sizeof(Request), 0, (struct sockaddr *)
				&sendToAddr, sizeof(servAddr));
		for(int x = 0; x < 9; x++)	//Attempts sending 10 times
		{
			for(int y = 0; y < 10; y++)	//Attempts reading 10 times (once every 100 millisec) before re-sending
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(READTIMEOUT));

				if(strlen(respBuffer) == 0){
					fromSize = sizeof(fromAddr);
					recvfrom(sock, respBuffer, 80, MSG_DONTWAIT, (struct sockaddr *) &fromAddr, &fromSize);
				}else{
					printf("Response recieved: "); printf(respBuffer); printf("\n");
					return;
				}
			}
			printf("Response timeout... resending...\n");
			sendto(sendtosock, &info, sizeof(Request), 0, (struct sockaddr *)
					&sendToAddr, sizeof(servAddr));
	    }
		printf("Failed to receive a response 10 times in a row...\n");
		printf("Exiting...\n");
		exit(0);
	    return;
}


void request_lseek(Request info, int sock, sockaddr_in servAddr)
{
	// DEBUG CODE
	debug_printRequest(info);
	printf("request_lseek() called.\n");
	char respBuffer[81];
		respBuffer[0] = 0;
		int test;
		unsigned int fromSize;
		struct sockaddr_in fromAddr;
		printf("Sending request...\n");
		sendto(sendtosock, &info, sizeof(Request), 0, (struct sockaddr *)
				&sendToAddr, sizeof(servAddr));
		for(int x = 0; x < 9; x++)	//Attempts sending 10 times
		{
			for(int y = 0; y < 10; y++)	//Attempts reading 10 times (once every 100 millisec) before re-sending
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(READTIMEOUT));

				if(strlen(respBuffer) == 0){
					fromSize = sizeof(fromAddr);
					recvfrom(sock, respBuffer, 80, MSG_DONTWAIT, (struct sockaddr *) &fromAddr, &fromSize);
				}else{
					printf("Response recieved: "); printf(respBuffer); printf("\n");
					return;
				}
			}
			printf("Response timeout... resending...\n");
			sendto(sendtosock, &info, sizeof(Request), 0, (struct sockaddr *)
					&sendToAddr, sizeof(servAddr));
	    }
		printf("Failed to receive a response 10 times in a row...\n");
		printf("Exiting...\n");
		exit(0);
	    return;
}


char* getIPAddress()
{
    struct ifaddrs *ifaddr, *ifa;
    int n, s;
    char host[NI_MAXHOST];
    if (getifaddrs(&ifaddr) == -1)
    {
        printf("Error in fetching interface addresses.");
        return NULL;
    }

    for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++) 
    {
        if (ifa->ifa_addr == NULL)
            continue;
        int family = ifa->ifa_addr->sa_family;

        if (family == AF_INET ) 
        {
            s = getnameinfo(ifa->ifa_addr,
                sizeof(struct sockaddr_in),
                host, NI_MAXHOST,
                NULL, 0, NI_NUMERICHOST);
            if (s != 0) {
               printf("getnameinfo() failed: %s\n", gai_strerror(s));
               exit(EXIT_FAILURE);
            }
            
            if (!(host[0] == '1' && host[1] == '2' && host[2] == '7'))
            {
                printf("\n\t\tfamily: <%s>\n", (family == AF_INET ? "AF_INET" : "AF_INET6"));
                printf("\t\taddress: <%s>\n", host);
            }
        }
    }
    char* hhost = new char[NI_MAXHOST];
    strcpy(hhost, host);
    return hhost;
}

int get_incarnation(Request inRequest){
	std::string filename = "";
	filename += inRequest.m;
	filename += "_incarnation";
	if (access(filename.c_str(), F_OK ) != -1) { // file exists, read
		//flock(file, LOCK_EX);	// broken flock lock
		std::ifstream oldIncFile;
		std::string word;
		oldIncFile.open(filename.c_str());
		oldIncFile >> word;
		//flock(file, LOCK_UN);	// broken flock release
		oldIncFile.close();
		return atoi(word.c_str());

	} else {	// file doesn't exist, create and set to 0
		std::ofstream newIncFile;
		newIncFile.open(filename.c_str());
		newIncFile << "0";
		newIncFile.close();
		return 0;
	}
	return 0;
}

int inc_incarnation(Request inRequest){
	std::string filename = "";
	filename += inRequest.m;
	filename += "_incarnation";
	int tempnum;
	std::string oldnum;

	// read, and increment
	std::ifstream incFileRead;
	incFileRead.open(filename.c_str());
	incFileRead >> oldnum;
	tempnum = atoi(oldnum.c_str());
	tempnum++;
	incFileRead.close();

	// open, write, and close
	std::ofstream incFileWrite;
	incFileWrite.open(filename.c_str());
	incFileWrite << tempnum;
	incFileWrite.close();

	return tempnum;
}

//DEBUGING FUCTION - Prints out all feilds of the given request struct in a nice format
void debug_printRequest(Request inRequest){
	printf("DEBUG: Printing current request state:\n");
	printf("client_ip:\t");printf(inRequest.client_ip);printf("\n");
	printf("\tm:\t");printf(inRequest.m);printf("\t\t--machine name\n");
	printf("\tc:\t");printf("%i", inRequest.c);printf("\t\t--client number\n");
	printf("\tr:\t");printf("%i", inRequest.r);printf("\t\t--request number\n");
	printf("\ti:\t");printf("%i", inRequest.i);printf("\t\t--incarnation number\n");
	printf("operation:\t");printf(inRequest.operation);printf("\n");
}


//////////////
//   MAIN
//////////////

int main(int argc, char* argv[])
{
	std::string word;
	Request tempRequest;    // request structure that is edited and sent to server


	/* Check for correct number of arguments */
	if(argc != 6)
	{
		fprintf(stderr, "Usage:\n\t%s <MachineName> <ClientNumber> <ServerIP> <PortNumber> <ScriptFile>\n", argv[0]);
		exit(1);
	}

	int sock;                        /* Socket descriptor */
	struct sockaddr_in servAddr; /* Echo server address */
	struct sockaddr_in fromAddr;     /* Source address of echo */

	unsigned short servPort;     /* Echo server port */
	unsigned int fromSize;           /* In-out of address size for recvfrom() */
	char *servIP;                    /* IP address of server */

	servIP = argv[3];           /* First arg: server IP address (dotted quad) */

	servPort = atoi(argv[4]);  /* Use given port, if any */

	/* Create a datagram/UDP socket */
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		DieWithError("[-]Socket() failed");

	/* Construct the server address structure */
	memset(&servAddr, 0, sizeof(servAddr));    /* Zero out structure */
	servAddr.sin_family = AF_INET;                 /* Internet addr family */
	//servAddr.sin_addr.s_addr = inet_addr(servIP);  /* Server IP address */
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servAddr.sin_port   = htons(servPort);     /* Server port */
	memcpy(&sendToAddr, &servAddr, sizeof(servAddr));

	// bind to local address
	int trycount = 0;	
	do {
		int bindresult = bind(sock, (struct sockaddr *) &servAddr, sizeof(servAddr));
		if (bindresult < 0)
		{
			printf("[-]Bind failed\n");
			
		} else 
		{
			printf("[+]Bind Succeeded!\n");
			sendtosock = sock;
			sendToAddr.sin_addr.s_addr = inet_addr(servIP);
			break;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(300));
		trycount++;
		if (trycount == 3)
		{
			trycount = 0;
			servPort++;
			servAddr.sin_port = htons(servPort);
			printf("[PORT]Limit reached, trying next port.\n");
		}
	} while (true);

//	if (trycount == 3)
//	{
//		servAddr.sin_port = htons(servPort+1);
//		int bindresult = bind(sock, (struct sockaddr *) &servAddr, sizeof(servAddr));
//		if (bindresult < 0)
//		{
//			printf("[-]Bind failed\n");
//			DieWithError("[-]Critical Error no socket to listen on. Exiting.\n");
//
//		} else
//		{
//			printf("[+]Bind Succeeded!\n");
//			if ((sendtosock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
//					DieWithError("[-]Socket() failed");
//			sendtosock = sock;
//			sendToAddr.sin_addr.s_addr = inet_addr(servIP);
//		}
//	}
	//servAddr.sin_addr.s_addr = inet_addr(servIP);
/*
	if(bind(sock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0){
		DieWithError("[-]Binding failed.\n");
	}else{
		printf("[+]Binding successful!\n");
	}
	printf("[+]Shell client created!\n");
*/	

	/* Set permanent elements inside of tempRequest */
	strcpy(tempRequest.m, argv[1]);// set client name in tempRequest
	tempRequest.c = atoi(argv[2]);	// set client number in tempRequest
	tempRequest.r = 0;	// request number stars at 0
	tempRequest.i = get_incarnation(tempRequest);

	/*
	 * these next two lines are weird. 2 hours of searching says this is how to get the IP address
	 * that was assigned a bit earlier in the line:
	 * 		servAddr.sin_addr.s_addr = htonl(INADDR_ANY);	// any incoming interface
	 * but it always prints 0.0.0.0, at least on my machine.
	 * but honestly, I don't even understand why we need our IP in the struct...
	*/
	//inet_ntop(AF_INET, &(servAddr.sin_addr.s_addr), tempRequest.client_ip, INET_ADDRSTRLEN);
	//printf(tempRequest.client_ip);	// seems to always print out 0.0.0.0 for some reason
	// Fixed Code:
	std::string tempip;
	memset(tempRequest.client_ip
			, '\0'
			, sizeof(tempRequest.client_ip)); // clear operation
	tempip += getIPAddress();
	tempip.copy(tempRequest.client_ip
			, tempip.length()
			, 0);
	printf("IP set:");
	printf(tempRequest.client_ip);


	/* open file for reading and check if successful */
	std::ifstream infile(argv[5]);
	if(!infile.is_open())
	{
	    fprintf(stderr, "ERROR: Could not open script file '%s'\n", argv[5]);
	    exit(1);
	}

	/* Iterate through the file word by word */
	while(infile >> word) // loop until end of file
	{

		printf("\n\n");
        if(word == "open")
        {
            printf("'open' command read.\n");
            memset(tempRequest.operation
            		, '\0'
            		, sizeof(tempRequest.operation));	// clear operation
            
            std::string tmpop;
            tmpop += "open ";
            infile >> word;
            tmpop += word + " ";
            infile >> word;
            tmpop += word;
            tmpop.copy(tempRequest.operation
                , tmpop.length()
                , 0);
            request_open(tempRequest, sock, servAddr);
            tempRequest.r++;	// increment request number by 1 after each request completion
        }
        else if(word == "close")
        {
            printf("'close' command read.\n");
            memset(tempRequest.operation
            		, '\0'
            		, sizeof(tempRequest.operation)); // clear operation

            std::string tmpop;
            tmpop += "close ";
            infile >> word;
            tmpop += word;
            tmpop.copy(tempRequest.operation
            		, tmpop.length()
            		, 0);
            request_close(tempRequest, sock, servAddr);
            tempRequest.r++;	// increment request number by 1 after each request completion

        }
        else if(word == "read")
        {
            printf("'read' command read.\n");
            memset(tempRequest.operation
            		, '\0'
            		, sizeof(tempRequest.operation)); // clear operation

            std::string tmpop;
            tmpop += "read ";
            infile >> word;
            tmpop += word + " ";
            infile >> word;
            tmpop += word;
            tmpop.copy(tempRequest.operation
            		, tmpop.length()
            		, 0);
            request_read(tempRequest, sock, servAddr);
            tempRequest.r++;	// increment request number by 1 after each request completion

        }
        else if(word == "write")
        {
            printf("'write' command read.\n");
			memset(tempRequest.operation
					, '\0'
					, sizeof(tempRequest.operation)); // clear operation

			std::string tmpop;
			tmpop += "write ";
			infile >> word;
			tmpop += word + " ";
            char* buf = new char[75];
			infile.getline(buf, 75);
			tmpop += std::string(buf);
			tmpop.copy(tempRequest.operation
					, tmpop.length()
					, 0);
            delete[] buf;
            request_write(tempRequest, sock, servAddr);
            tempRequest.r++;	// increment request number by 1 after each request completion

        }
        else if(word == "lseek")
        {
            printf("'lseek' command read.\n");

			memset(tempRequest.operation
					, '\0'
					, sizeof(tempRequest.operation)); // clear operation

			std::string tmpop;
			tmpop += "lseek ";
			infile >> word;
			tmpop += word + " ";
			infile >> word;
			tmpop += word;
			tmpop.copy(tempRequest.operation
					, tmpop.length()
					, 0);
            request_lseek(tempRequest, sock, servAddr);
            tempRequest.r++;	// increment request number by 1 after each request completion

        }
        else if(word == "fail")
        {
            printf("'fail' command read.\n");
            tempRequest.r++;	//increment request number by 1 after each request completion
            tempRequest.i = inc_incarnation(tempRequest);

        }
        else
        {
            printf("ERROR: Invalid command in script file '%s'\n", word.c_str());
            exit(1);
        }

	}

    printf("Program exiting normally.");
	return 0;
}
