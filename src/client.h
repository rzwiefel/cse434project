///////////////////////////////////////
// client.h
///////////////////////////////////////


///////////////////////////////////////
// INCLUDES & DEFINES
///////////////////////////////////////

#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <netdb.h>
#include <ifaddrs.h>
#include <sys/file.h>
#include <thread>
#include <chrono>

#define ECHOMAX 255
#define READTIMEOUT 100

///////////////////////////////////////
// DECLARATIONS
///////////////////////////////////////

int main(int, char**);

/* Request structure specified by the Professor */
struct Request{
    char client_ip[16]; // Holds client IP address in dotted decimal
    char m[24]; // Name of machine on which the client is running
    int c;  // Client number
    int r;  // request number of client
    int i;  // Incarnation number of client's machine
    char operation[80]; // File operation client sends to server
};

void DieWithError(const char);
void request_open(Request);
void request_close(Request);
void request_read(Request);
void request_write(Request);
void request_lseek(Request);
char* getIPAddress();
int get_incarnation(Request);
int inc_incarnation(Request);

//debug fuctions:
void debug_printRequest(Request);

int sendtosock;
struct sockaddr_in sendToAddr;
