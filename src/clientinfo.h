// clientinfo.cpp
#include <vector>

#ifndef REQUEST_STRUCT
#define REQUEST_STRUCT
struct Request{
    char client_ip[16]; // Holds client IP address in dotted decimal
    char m[24]; 		// Name of machine on which the client is running
    int c;  			// Client number
    int r;  			// request number of client
    int i;  			// Incarnation number of client's machine
    char operation[80]; // File operation client sends to server
};
#endif

#ifndef RESPONSE_STRUCT
#define RESPONSE_STRUCT
struct Response
{
	int responseNumber;
	char guid[37];
	char response[80];
};
#endif

//////////////////////////
// client info struct
//////////////////////////

struct clientinfo {
	char clientIP[16];
	char clientName[24];
	int incarnationNumber;
	int clientNumber;
	int requestNumber;
	std::vector<Request*> *requestList;
	std::vector<char*> *responseList;
	struct sockaddr_in *clientAddr;
	struct thread_info *thread;
	struct lockinfo *lock;
	~clientinfo()
	{
		delete clientAddr;
		for (auto& req : *requestList) delete req;
		requestList->clear();
		delete requestList;
		for (auto& res : *responseList) delete res;
		responseList->clear();
		delete responseList;

	}
};


