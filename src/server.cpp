// server.cpp
#include "server.h"

void DieWithError(const char *errorMessage) // error handling: print & exit
{
	perror(errorMessage);
	exit(1);
}

int getRequestAction()
{
	int randnum = rand() % 3; //Generate number between 0-2
	if (randnum == PROCESSREQ) 
	{
		printf("[SIMULATE]Proceeding with requested action (PROCESSREQ).\n");
	}
	if (randnum == DROPRESPONSE)
	{
		printf("[SIMULATE]Proceeding with requested action, but dropping response(DROPRESONSE).\n");
	}
	if (randnum == FAILRECV)
	{
		printf("[SIMULATE]Failing to receive response, ignoring(FAILRECV).\n");
	}

	return randnum;
}

void handleResponse()
{
	clientInfoTable;
	/*Search for entry in clientTable*/
	int action = 0;
	int loc = -1;
	for(int i = 0; i < clientInfoTable->size(); i++)
	{
		if (strcmp(clientInfoTable->at(i)->clientName, request.m) == 0
				&& clientInfoTable->at(i)->clientNumber == request.c)
		{
			printf("[HandleResponse] Found client info in table!\n");
			loc = i;
			break;
		}
	}
	/*if client not in table, add it*/
	if (loc == -1)
	{
		printf("[HandleResponse] Client not found, creating new entry.\n");
		loc = addNewClient();
	}

	/*Copy the client location information if it's not there*/
	printf("[HandleResponse] Copying client address location.\n");
	copyClientAddress(loc);

	/*Copy the request from memory and add it to the clientTable entries requestList after search*/
	struct Request* tmpreq = new Request();
	memcpy(tmpreq, &request, sizeof(request));
	auto clientRequestList = clientInfoTable->at(loc)->requestList;
	auto clientResponseList = clientInfoTable->at(loc)->responseList;
	/*Search the clients information and check if the current request already has a response or not*/
	/* make sure there is more than the request we just put in there to search against*/
	printf("[HandleResponse] Checking incoming request and analyzing.\n");
	printf("[HandleResponse] clientRequestList.size: %i\n", clientRequestList->size());
	if (clientRequestList->size() > 0)
	{
		printf("[HandleResponse] clientRequestList has some items to compare against.\n");
		if (clientRequestList->back()->i != tmpreq->i)
		{
			printf("[-]Incarnation number of incoming request is higher than previous, detecting client failure. Returning invalid.\n");
			action = INVALIDATERESPONSE;
		} else if (clientRequestList->back()->r > tmpreq->r)
		{
			printf("[-]Ignoring request since request number is lower than previous request.\n");
			action = IGNORERESPONSE;
		} else if (clientRequestList->back()->r == tmpreq->r)
		{
			printf("[+]Request was already processed, sending processed response\n");
			action = RESENDRESPONSE;
		} else
		{
			printf("[+]Processing request.\n");
			action = PROCESSRESPONSE;
		}
	} else action = PROCESSRESPONSE;
	char *tmpres;
	printf("[Action] Action number is: %i\n", action);
	switch (action)
	{
	case INVALIDATERESPONSE:
		/*Release client first since they are invalid now*/
		releaseClient(loc);
		/*Now start a fresh connection*/
		loc = addNewClient();
		copyClientAddress(loc);
		tmpres = processResponse(tmpreq, clientInfoTable->at(loc), loc);
		clientResponseList->push_back(tmpres);
		if (simulateStatus != DROPRESPONSE)
		{
			sendResponse(clientResponseList->back(), clientInfoTable->at(loc));
		}
		break;
	case IGNORERESPONSE:
		break;
	case RESENDRESPONSE:
		/*make sure there is a stored response to send*/
		if (clientResponseList->size() > 0)
		{
			if (simulateStatus != DROPRESPONSE)
			{
				printf("[RESP] Resending response.\n");
				sendResponse(clientResponseList->back(), clientInfoTable->at(loc));
			} else
			{
				return;
			}
		}
		break;
	case PROCESSRESPONSE:
		printf("[RESP] Processing response.\n");
		clientRequestList->push_back(tmpreq);
		tmpres = processResponse(tmpreq, clientInfoTable->at(loc), loc);
		clientResponseList->push_back(tmpres);
		printf("[RESP] Processing finished.\n");
		if (simulateStatus != DROPRESPONSE)
		{
			// printf("[SOCK] calling sendResponse method.\n");
			sendResponse(clientResponseList->back(), clientInfoTable->at(loc));
		}

		break;
	}
}

void sendResponse(char *_response, clientinfo *_addr)
{
	printf("Sending response [%s] to client [%s]\n", _response, _addr->clientIP);

	 struct sockaddr_in cliaddr;
	 memset(&cliaddr, 0, sizeof(cliaddr));    /* Zero out structure */
	 cliaddr.sin_family = AF_INET;                 /* Internet addr family */
	 cliaddr.sin_addr.s_addr = inet_addr(_addr->clientIP);   /*Server IP address */
	 printf("Server port using [%i] client port using [%i]\n", servPort, ntohs(_addr->clientAddr->sin_port));
	 if (strcmp(_addr->clientIP, myip) == 0
			 || htons(servPort) != _addr->clientAddr->sin_port)
	 {
	 	printf("[SOCK]Detecting port variation, using backup port\n");
	 	cliaddr.sin_port = _addr->clientAddr->sin_port;
	 } else
	 {
	 	cliaddr.sin_port   = _addr->clientAddr->sin_port;
	 }

	int sendlen = sendto(
			sock
			, _response
			, MAXRESPONSELEN
			, 0
			, (struct sockaddr *)&cliaddr	//_addr->clientAddr
			, sizeof(struct sockaddr_in));
	printf("The length sent through the socket was: %i\n", sendlen);
}

char* processResponse(Request *_request, clientinfo *clin, int cli)
{
	std::string reqstr = std::string(_request->operation);
	std::string cur;
	std::stringstream rss(reqstr);
	char* response = new char[MAXRESPONSELEN];	
	rss >> cur;

	if (cur == "open")
	{
		printf("Open command read.\n");
		lockInfoTable->push_back(copyLockInfo(clin));
		refreshTime(clin);
		rss >> cur;
		strcpy(clin->lock->filename, cur.c_str());
		// std::string fname = cur;
		rss >> cur;
		if (cur == "read") clin->lock->lockType = READLOCK;
		if (cur == "write") clin->lock->lockType = WRITELOCK;
		if (cur == "readwrite") clin->lock->lockType = READWRITE;
		std::string fname = std::string(clin->lock->clientName);
		fname += ":" + std::string(clin->lock->filename);
		printf("[open] filename being set as: %s\n", fname.c_str());
		if (clin->lock->lockType & READLOCK)
		{
			clin->lock->infile = new std::ifstream(fname);
		}
		if (clin->lock->lockType & WRITELOCK)
		{
			clin->lock->outfile = new std::ofstream(fname);
		}
		std::string _resp = "open ";
		_resp += std::string(clin->lock->filename);
		printf("Response is: %s\n", _resp.c_str());
		refreshTime(clin);
		strcpy(response, _resp.c_str());
		printf("Response in mem is: %s\n", response);
	} else if (cur == "close")
	{
		printf("Close command read.\n");
		rss >> cur;
		releaseClient(cli);
		std::string _resp = "close " + cur;
		strcpy(response, _resp.c_str());
	} else if (cur == "read")
	{
		printf("Read command read.\n");
		refreshTime(clin);
		rss >> cur;
		rss >> cur;
		int rlen = atoi(cur.c_str()) % MAXRESPONSELEN;
		if (!(clin->lock->lockType & READLOCK))
		{
			printf("[!] Read command error -> File not opened for reading.\n");
			strcpy(response, "read [ERROR] not open for reading");
			return response;
		}
		if (clin->lock == NULL || clin->lock->infile == NULL)
		{
			printf("[!] Read command error -> client owns no files currently. [file:%s]\n", cur.c_str());
			strcpy(response, "read [ERROR] File not open");
			return response;
		}

		char *buf = new char[rlen];
		clin->lock->infile->read(buf, rlen);
		std::string bufstr = std::string(buf);
		std::string _resp = "read " + bufstr;
		refreshTime(clin);
		printf("[read] content read was: %s\n", _resp.c_str());
		strcpy(response, _resp.c_str());	
		delete[] buf;
	} else if (cur == "write")
	{
		printf("Write command read.\n");
		refreshTime(clin);
		rss >> cur;
		printf("[write] writing to file: %s\n", cur.c_str());
		std::string body;
		while (rss >> cur)
		{
			body += cur + " ";
		}
		body.erase(body.find_last_not_of(" \n\r\t")+1);
		body = body.substr(1, body.length()-2);
		printf("[write] String to write is: %s\n", body.c_str());
		if (!(clin->lock->lockType & WRITELOCK))
		{
			printf("[!] Write command error -> File not opened for writing.\n");
			strcpy(response, "write [ERROR] not open for writing");
			return response;
		}
		if (clin->lock == NULL || clin->lock->outfile == NULL)
		{
			printf("[!] Write command error -> client owns no files currently. [file:%s]\n", cur.c_str());
			strcpy(response, "write [ERROR] File not open");
			return response;
		}
		clin->lock->outfile->write(body.c_str(), body.length());
		std::string _resp = "write " + body;
		strcpy(response, _resp.c_str());
		refreshTime(clin);
	} else if (cur == "lseek")
	{
		printf("Lseek command read.\n");
		refreshTime(clin);
		rss >> cur;
		printf("[lseek] seeking in file: %s\n", cur.c_str());
		rss >> cur;
		int slen = atoi(cur.c_str());
		if (clin->lock == NULL)
		{
			printf("[!] Lseek command error -> client owns no files currently. [file:%s]\n", cur.c_str());
			strcpy(response, "lseek [ERROR] File not open");
			return response;
		}
		printf("[lseek] attempting to seek [%s] bytes\n", cur.c_str());
		if (clin->lock->infile != NULL)
			clin->lock->infile->seekg(slen);
		if (clin->lock->outfile != NULL)
			clin->lock->outfile->seekp(slen);
		std::string _resp = "lseek " + cur;
		refreshTime(clin);
		strcpy(response, _resp.c_str());
	} else 
	{
		printf("Invalid command read.\n");
		strcpy(response, "invalid command");
	}
	return response;
}

int addNewClient() 
{
	clientinfo* ci = new clientinfo();
	memset(ci, 0, sizeof(clientinfo));
	strcpy(ci->clientIP, request.client_ip);
	strcpy(ci->clientName, request.m);
	ci->clientNumber = request.c;
	ci->requestNumber = request.r;
	ci->incarnationNumber = request.i;
	ci->clientAddr = NULL;
	ci->requestList = new std::vector<Request*>();
	ci->responseList = new std::vector<char*>();
	clientInfoTable->push_back(ci);
	printf("[AddNewClient] New client added at position: %i\n", clientInfoTable->size()-1);
	return clientInfoTable->size() - 1;
}

void releaseClient(int clientTableIndex)
{
	releaseClient(clientTableIndex, false);
}

void releaseClient(int clientTableIndex, bool i_am_thread)
{
	clientinfo *clin = clientInfoTable->at(clientTableIndex);
	if (clin->clientNumber != -1)
	{
		printf("!Releasing client! [%s:%i]\n", clin->clientName, clin->clientNumber);
		int idx = 0;
		char* cliname = clin->clientName;

		/*Delete the lock info entry*/
		for (auto& lock : *lockInfoTable)
		{
			if (strcmp(cliname, lock->clientName) == 0 && clin->lock != NULL)
			{
				if (lock == clin->lock)
				{
					printf("[+]Found lock info entry && deleting.\n");
					if (lock->infile != NULL && lock->infile->is_open())
						lock->infile->close();
					if (lock->outfile != NULL && lock->outfile->is_open())
						lock->outfile->close();
					delete lock;
					lockInfoTable->erase(lockInfoTable->begin() + idx);
					break;
				}
			}
			idx++;
		}
		clin->lock = NULL;
		printf("Marking entry for kill.\n");
		/*Mark the client table entry for delete by the thread*/
		if (clin->thread != NULL)
			clin->thread->kill = true;
		/*Set the client num to -1 so it doesn't belong to same client anymore*/
		clin->clientNumber = -1;
	}
	/*If I am thread, then i can delete the client info table*/
	if (i_am_thread)
	{
		printf("[i_am_thread] Deleting clientTableEntry and erasing from table.\n");
		delete clin;
		printf("[i_am_thread] Table entry index: %i\n", clientTableIndex);
		clientInfoTable->erase(clientInfoTable->begin() + clientTableIndex);
	}
}

void copyClientAddress(int loc)
{
	if (clientInfoTable->at(loc)->clientAddr == NULL)
	{
		struct sockaddr_in *tmpclientAddr = new struct sockaddr_in();
		memcpy(tmpclientAddr, &clientAddr, sizeof(struct sockaddr_in));
		clientInfoTable->at(loc)->clientAddr = tmpclientAddr;
	}
}

void timeoutThread(thread_info* info, clientinfo* clin)
{
	std::this_thread::sleep_for(std::chrono::microseconds(5));
	std::thread* me = info->me;
	printf("[Thread] New thread created for %s.\n", clin->clientName);
	while (info->expiry - time(0) > 0)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		if (info->kill) break;
	}
	int idx = 0;
	for (auto& cli : *clientInfoTable)
	{
		if (cli == clin)
			break;
		idx++;
	} 
	if (idx >= clientInfoTable->size())
	{
		printf("[CRITICAL ERROR] client wasn't found.\n");
		printf("[CRITICAL ERROR] possible induced memory leak now.\n");
		return;
	}
	releaseClient(idx, true);
	printf("[Thread] Thread ending, goodbye i will miss you.\n");
	join_me.push_back(me);
}

void refreshTime(clientinfo* clin)
{
	// DEBUG
	// return;
	// DEBUG
	if (clin->thread == NULL)
	{
		clin->thread = new struct thread_info();
		memset(clin->thread, 0, sizeof(thread_info));
		clin->thread->kill = false;
		clin->thread->expiry = time(0) + 10;
		clin->thread->me = new std::thread(timeoutThread, clin->thread, clin);
		// clin->thread->me->detach();
	}
	clin->thread->expiry = time(0) + 10;
	printf("[ThreadController] Timeout refreshed [name:%s] [%i]\n", clin->clientName,clin->thread->expiry-time(0));
}

lockinfo* copyLockInfo(clientinfo* clin)
{
	lockinfo *_lock = new lockinfo();
	_lock->infile = NULL;
	_lock->outfile = NULL;
	memcpy(_lock->clientName,  clin->clientName, 24);
	clin->lock = _lock;
	return _lock;
}

lockinfo* lockSearch(char* filename, char* clientname)
{
	lockinfo *result = NULL;
	for (auto& lock : *lockInfoTable)
	{
		if (strcmp(lock->filename, filename) == 0
				&& strcmp(lock->clientName, clientname) == 0)
		{
			result = lock;
			break;
		}
	}
	return result;
}

int lockSearchIdx(char* filename, char* clientname)
{
	int result = -1;
	int idx = 0;
	for (auto& lock : *lockInfoTable)
	{
		if (strcmp(lock->filename, filename) == 0
				&& strcmp(lock->clientName, clientname) == 0)
		{
			result = idx;
			break;
		}
		idx++;
	}
	return result;
}

void joiner()
{
	printf("[Thread Gobbler]Waiting for lonely threads!\n");
	while (join)
	{
		std::this_thread::sleep_for(std::chrono::seconds(10));
		if (join_me.size() > 0)
		{
			for (auto& thr : join_me)
			{
				if (thr->joinable())
				{
					printf("[Thread Gobbler] Thread, fear not! ");
					thr->join();
					printf("I accept you!\n");
				}
			}
		}
	}
}

char* getIPAddress()
{
	struct ifaddrs *ifaddr, *ifa;
	int n, s;
	char host[NI_MAXHOST];
	if (getifaddrs(&ifaddr) == -1)
	{
		printf("Error in fetching interface addresses.");
		return NULL;
	}

	for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++)
	{
		if (ifa->ifa_addr == NULL)
			continue;
		int family = ifa->ifa_addr->sa_family;

		if (family == AF_INET )
		{
			s = getnameinfo(ifa->ifa_addr,
					sizeof(struct sockaddr_in),
					host, NI_MAXHOST,
					NULL, 0, NI_NUMERICHOST);
			if (s != 0) {
				printf("getnameinfo() failed: %s\n", gai_strerror(s));
				exit(EXIT_FAILURE);
			}

			if (!(host[0] == '1' && host[1] == '2' && host[2] == '7'))
			{
				printf("\n\t\tfamily: <%s>\n", (family == AF_INET ? "AF_INET" : "AF_INET6"));
				printf("\t\taddress: <%s>\n", host);
			}
		}
	}
	char* hhost = new char[NI_MAXHOST];
	strcpy(hhost, host);
	return hhost;
}

//DEBUGING FUCTION - Prints out all feilds of the given request struct in a nice format
void debug_printRequest(Request inRequest){
	printf("DEBUG: Printing current request state:\n");
	printf("client_ip:\t");printf(inRequest.client_ip);printf("\n");
	printf("\tm:\t");printf(inRequest.m);printf("\t\t--machine name\n");
	printf("\tc:\t");printf("%i", inRequest.c);printf("\t\t--client number\n");
	printf("\tr:\t");printf("%i", inRequest.r);printf("\t\t--request number\n");
	printf("\ti:\t");printf("%i", inRequest.i);printf("\t\t--incarnation number\n");
	printf("operation:\t");printf(inRequest.operation);printf("\n");
}

////////////////////////////////
// MAIN FUNCTION
////////////////////////////////

int main(int argc, char* argv[])
{

	////////////////////////////////
	// SOCKET CREATION AND BINDING
	////////////////////////////////
	join = true;
	char* tip = getIPAddress();
	strcpy(myip, tip);
	delete[] tip;
	clientInfoTable = new std::vector<clientinfo*>();
	lockInfoTable = new std::vector<lockinfo*>();


	/* Check for correct number of arguments */
	if(argc != 2)
	{
		fprintf(stderr, "Usage:\n\t%s <Port Number>\n", argv[0]);
		exit(1);
	}
	servPort = atoi(argv[1]); // set port to first arg

	/*initialize random seed for simulation*/
	srand(time(NULL));

	/* Socket creation -- exit if creation fails*/
	if((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0){
		DieWithError("[-]Socket creation failed.\n");
	}else{
		printf("[+]Socket created successfully!\n");
	}

	/* Construct local address structure */
	memset(&serverAddr, 0, sizeof(serverAddr));		// zero out structure
	memset(&request, 0, sizeof(request));
	serverAddr.sin_family = AF_INET;			// internet address family
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);	// any incoming interface
	serverAddr.sin_port = htons(servPort);		// local port

	/* bind to local address*/
	if(bind(sock, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0){
		DieWithError("[-]Binding failed.\n");
	}else{
		printf("[+]Binding successful!\n");
	}
	printf("[+]Shell server created!\n");
	std::thread jointhr(joiner);
	std::this_thread::sleep_for(std::chrono::microseconds(10));
	/////////////////////////////
	// SOCKET CREATED AND BOUND
	/////////////////////////////

	/////////////////////////////
	// BEGIN MESSAGE RECV LOOP
	/////////////////////////////
	printf("[+]Listening for incoming requests...\n");
	while (true)
	{
		memset(&request, 0, sizeof(request)); //TODO: DO i need to wipe this struct clean?
		memset(&clientAddr, 0, sizeof(clientAddr));
		clientAddrLen = sizeof(clientAddr);

		if ((recvMsgSize = recvfrom(sock, &request, sizeof(request), 
				0, (struct sockaddr *) &clientAddr, &clientAddrLen)) != sizeof(request))
		{
			// DieWithError("[-]Error receiving message.\n");
			printf("ERROR RECEIVING MESSAGE!! MESSAGE SIZE: %i\n", recvMsgSize);
			char* te = new char[recvMsgSize];
			for (int i = 0; i < recvMsgSize; i++)
			{
				if ((int)te[i] == 0) printf("[NULL]");
				else if ((int)te[i] > 31 && (int)te[i] < 127) printf("%c", te[i]);
				else printf("?");
			}
			printf("\n");
		}
		printf("\n\nMessage recieved!\n");
		// debug_printRequest(request);
		simulateStatus = getRequestAction(); //randomly generate action
		if (simulateStatus == FAILRECV)
		{
			//If simulate drop response, we continue as if we
		}	else		//never got the message
		{
			handleResponse();
		}
	}
	printf("[End of code]\n");
	return 0;
}
