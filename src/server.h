///////////////////////////////////////
// server.h
///////////////////////////////////////

///////////////////////////////////////
// INCLUDES & DEFINES
///////////////////////////////////////

#include <arpa/inet.h>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <sys/socket.h>
#include <thread>
#include <time.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <unistd.h>
#include <vector>
#include <mutex>
#include <unordered_map>
#include "clientinfo.h"
#include "lockinfo.h"

#define MAXRESPONSELEN 80
#define PROCESSREQ 0 	//Execute request normally
#define DROPRESPONSE 1	//Process request, but drop response
#define FAILRECV 2		//Do not process request

#define PROCESSRESPONSE 1
#define RESENDRESPONSE 2
#define INVALIDATERESPONSE 3
#define IGNORERESPONSE 4

///////////////////////////////////////
// Declarations
///////////////////////////////////////

/* Request structure specified by the Professor */
#ifndef REQUEST_STRUCT
#define REQUEST_STRUCT
struct Request{
	char client_ip[16]; // Holds client IP address in dotted decimal
	char m[24]; 		// Name of machine on which the client is running
	int c;  			// Client number
	int r;  			// request number of client
	int i;  			// Incarnation number of client's machine
	char operation[80]; // File operation client sends to server
};
#endif

struct thread_info
{
	bool kill;
	time_t expiry;
	std::thread *me;
};

/* I guess this is a thread for another day. RIP my effort to figure this out*/
//class my_thread : public std::thread
//{
//public:
//	template <class Fn, class...Args>
//	my_thread(Fn&& fn, Args&&... args) : std::thread(fn, args...)
//	{
//		_kill = false;
//		time(&expiry);
//		expiry += 10;
//	};
//
//	bool _kill;
//	time_t expiry;
//};

//Function Declarations
int main(int, char**);
int getRequestAction();
void handleResponse();
int addNewClient();
void sendResponse(char*, clientinfo*);
void releaseClient(int);
void releaseClient(int, bool);
char* processResponse(Request*, clientinfo*, int cli);
void copyClientAddress(int);
void timeoutThread(thread_info*, clientinfo*);
void refreshTime(clientinfo*);
lockinfo* copyLockInfo(clientinfo*);
lockinfo* lockSearch(char*, char*);
int lockSearchIdx(char*, char*);
void joiner();
char* getIPAddress();
void debug_printRequest(Request);

//Local Variables
int sock;			// socket
unsigned short servPort;	// server port
struct sockaddr_in serverAddr;	// local IP
struct sockaddr_in clientAddr;
struct Request request;
uint clientAddrLen;
int simulateStatus;
int recvMsgSize;
bool join;
char myip[16];
std::vector<clientinfo*> *clientInfoTable;
std::vector<lockinfo*> *lockInfoTable;

std::vector<std::thread*> join_me;
// std::vector<std::thread*> resendTimeoutThreads;
//std::unordered_map<int, thread_info> tmap;
//std::mutex mtx;

//limit ry_re_re_cl_re_al_cl
