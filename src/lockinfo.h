// lockinfo.cpp

///////////////////////////
// Defines
//////////////////////////
#define UNLOCKED 0
#define READLOCK 1
#define WRITELOCK 2
#define READWRITE 3

///////////////////////////
//
///////////////////////////


struct lockinfo {
	int lockType;
	char clientName[24];
	char filename[24];
	std::ifstream *infile;
	std::ofstream *outfile;
};

