#CLIENT PROGRAM
CLIENTNAME = client
CLIENTFILES = src/client.cpp src/client.h

#SERVER PROGRAM
SERVERNAME = server
SERVERFILES = src/server.cpp src/server.h 
#src/clientinfo.cpp src/clientlock.cpp

#COMPILER SETTINGS
CXX = g++
CXXFLAGS = -w -pthread -std=c++11 -Wl,--no-as-needed

#MAKE
all: client server

client:
	@echo Making client...
	$(CXX) $(CXXFLAGS) -o $(CLIENTNAME) $(CLIENTFILES)
	@echo ...client made.


server:
	@echo Making server...
	$(CXX) $(CXXFLAGS) -o $(SERVERNAME) $(SERVERFILES)
	@echo ...server made.

clean:
	rm -f $(CLIENTNAME) $(SERVERNAME)
